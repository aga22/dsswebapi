﻿using System;
using System.Web.Http;
using DSSWebAPI.Models;
using System.Threading;
using RDotNet;

namespace DSSWebAPI.Controllers {
	public class SolverController : ApiController {

		Model M = new Model();
		public string connString, factory;
		string dataDirectory = (string) AppDomain.CurrentDomain.GetData("DataDirectory");

		public SolverController() {
			Pair<string,string> p = Util.handleConnection(connString,factory,dataDirectory);
			connString = p.getFirstElem();
			factory = p.getSecondElem();
		}

		[HttpGet]
		[ActionName("SolveInstance")]
		public string SolveInstance(string selection,string param) {
			string res;
			GAPInstance GAP = new GAPInstance();
			res = M.solveInstance(selection,param);
			return res;
		}

		[HttpGet]
		[ActionName("GetSeasonality")]
		public int GetSeasonality(string selection,string param) {
			return selection == ("esempio2") ? 12 : PearsonSeasonality.findSeasonality(selection);
		}

		[HttpGet]
		[ActionName("GetSerie")]
		public IHttpActionResult GetSerie(string selection,string param) {
			if(param.Equals("serie")) {
				string queryText = "select " + selection + " from serie";
				string s = M.getSerie(connString,queryText,factory);
				switch(selection) {
					case "esempio":
						M.createCSVFile(selection,M.getListFromSerie(connString,queryText,factory),4,2004);
						break;
					case "esempio2":
						M.createCSVFile(selection,M.getListFromSerie(connString,queryText,factory),12,1992);
						break;
					case "jewelry":
						M.createCSVFile(selection,M.getListFromSerie(connString,queryText,factory),12,1997);
						break;
					default:
						M.createCSVFile(selection,M.getListFromSerie(connString,queryText,factory),12,1992);
						break;
				}
				if(s == null)
					return NotFound();
				return Ok(s);
			} else {
				NumericVector ret = null;
				switch(selection) {
					case "esempio":
						ret = RComputation(selection,GetSeasonality(selection,param) + "",GetSeasonality(selection,param) * 2 + "");
						break;
					default:
						ret = RComputation(selection,GetSeasonality(selection,param) + "",GetSeasonality(selection,param) * 2 + "");
						break;
				}
				return Ok(ret);
			}
		}

		private NumericVector RComputation(string s,string frequency,string h) {
			object ret = null;
			Thread t = new Thread(() => { ret = M.computerRScript(s,frequency,h); },2500000);
			t.Start();
			t.Join();
			return (NumericVector) ret;
		}
	}
}








