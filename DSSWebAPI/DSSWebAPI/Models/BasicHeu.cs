﻿using System;

namespace DSSWebAPI.Models {
	class BasicHeu {
		int n, m;
		GAPInstance GAP;
		double EPS;
		public int[] sol;
		public int[] capacitiesLeft;

		public BasicHeu(GAPInstance gap) {
			GAP = gap;
			n = GAP.numcli;
			capacitiesLeft = (int[]) GAP.cap.Clone();
			m = GAP.numserv;
			EPS = 1000;
		}
		/*
		 * Restituisce il costo di GAP per ogni client
		 */
		public int getCost(int[] sol) {
			if(!isSolValid(sol)) return Int32.MaxValue;

			int z = 0;
			for(int j = 0;j < GAP.numcli;j++)
				z += GAP.cost[sol[j],j];

			return z;
		}
		/*
		 * Verifica la validità della soluzione
		 * controllando la presenza di GAP, sol e 
		 * che ci siano i corretti numeri di client
		 * e server
		 */
		private bool isSolValid(int[] sol) {
			if(GAP == null || sol == null || GAP.numcli != sol.Length) return false;

			int[] capused = new int[GAP.numserv];
			for(int j = 0;j < n;j++) {
				if(sol[j] < 0 || sol[j] >= m)
					return false;

				capused[sol[j]] += GAP.req[sol[j],j];
				if(capused[sol[j]] > GAP.cap[sol[j]])
					return false;
			}
			return true;
		}

		/*
		 * Funzione che controlla il costo della soluzione
		 * proposta.
		 * 
		 */
		public int checkSol(int[] sol) {
			int z = 0, j;
			int[] capused = new int[m];
			for(int i = 0;i < m;i++) capused[i] = 0;
			for(j = 0;j < n;j++)
				if(sol[j] < 0 || sol[j] >= m) {
					return int.MaxValue;
				} else
					z += GAP.cost[sol[j],j];
			for(j = 0;j < n;j++) {
				capused[sol[j]] += GAP.req[sol[j],j];
				if(capused[sol[j]] > GAP.cap[sol[j]]) {
					return int.MaxValue;
				}
			}
			return z;
		}

		/*
		 * Soluzione euristica costruttiva;
		 * 1. Ordina i componenti in C per costi
			crescenti.
		   2. Set S*=empty e i=1.
		   3. Repeat
				If (S* unione c[i] è una soluz. parziale ammissibile)
			then S*=S*unione c[i].
				i=i+1.
			Until S*appartiene F
			Molte alternative possibili. Ad esempio si ordinano i
			clienti per regret decrescenti e poi li si assegna a
			turno, ordinando per ciascuno i magazzini per
			richieste crescenti.
		 */
		public int constructiveEurFirstSol() {
			sol = new int[n];
			int[] keys = new int[m];
			int[] index = new int[m];
			int[] capleft = capacitiesLeft;
			int ii;

			for(int j = 0;j < n;j++) {
				for(int i = 0;i < m;i++) {
					keys[i] = GAP.req[i,j];
					index[i] = i;
				}
				Array.Sort(keys,index);
				for(ii = 0;ii < m;ii++) {
					int i = index[ii];
					if(capleft[i] >= GAP.req[i,j]) {
						capleft[i] -= GAP.req[i,j];
						sol[j] = i;
						break;
					}
				}
				if(ii == m) {
					return -1;
				}
			}
			int z = checkSol(sol);
			GAP.zub = z;
			return z;
		}

		/*
		 * Soluzione GAP10;
		 *  1.Genera una soluzione iniziale ammissibile S.
			2. Trova S'in(S), tale che z(S')=min z(S^),
			"S^in(S).
			3. If z(S') < z(S) then S=S'
			goto step 2.
			4. S* = S.
			Si considera a turno ogni client e si prova
			a riassegnarlo ad ogni altro magazzino
			che ha capacità residua sufficiente.
		 */
		public int GAP10() {
			int isol = 0;
			int[] capleft = this.capacitiesLeft;
			int[,] cost = GAP.cost;
			int[,] req = GAP.req;
			int z = 0;
			bool isImproved = true;

			int k = this.constructiveEurFirstSol();
			for(int j = 0;j < n;j++) {
				z += cost[sol[j],j];
			}
			while(isImproved) {
				isImproved = false;
				for(int j = 0;j < n;j++) {
					for(int i = 0;i < m;i++) {
						isol = sol[j];
						if(i != isol && cost[i,j] < cost[isol,j] && capleft[i] >= req[i,j]) {
							isImproved = true;
							sol[j] = i;
							capleft[i] -= req[i,j];
							capleft[isol] += req[isol,j];
							z -= (cost[isol,j] - cost[i,j]);
							if(z < GAP.zub) {
								GAP.zub = z;
							}
						}
					}
				}
			}
			double zCheck = 0;
			for(int j = 0;j < n;j++) {
				zCheck += cost[sol[j],j];
			}
			if(Math.Abs(z - zCheck) > EPS) {
				return -1;
			}
			return z;
		}

		/*
		 *  Soluzione Simulated Annealing;
			Algoritmo derivato dalla termodinamica
			• Incorpora un parametro “temperatura” nella
			procedura di minimizzazione
			• Ad “alte temperature" diversifica l’esplorazione
			• A “basse temperature" intensifica l’esplorazione
			Considera serie di temperature decrescenti
			Ad ogni temperatura t:
			• propone una nuova soluzione e la valuta
			• accetta la soluzione se migliorativa
			• accetta qualche soluzione peggiorativa
			• la probabilità di accettazione dipende dal parametro
			“temperatura”
			Se il raffreddamento è sufficientemente lento, si
			raggiunge l’ottimo globale.
			1. Genera una soluzione iniziale ammissibile S,
			inizializza S* = S e il parametro temperatura T.
			2. Genera S’in(S).
			3. se z(S') < z(S) allora S=S', if (z(S*) > z(S)) S* = S
			altrimenti accetta di porre S=S' con probabilità
			p = e-(z(S')-z(S))/kT.
			4. se (annealing condition) cala T.
			5. se not(end condition) go to step 2
		 */
		public int[] computeSimulatedAnnealing(int[] sol) {
			int[] capLeft = this.capacitiesLeft;
			int[] optSol = (int[]) sol.Clone();
			int optCost = getCost(sol);

			int[] currentSol = (int[]) optSol.Clone();
			int currentCost = optCost;

			Random r = new Random();
			double p = 0.6;
			double? T = null;

			int totalSteps = 0, step = 0;

			while(step <= 50 || p > 0.0001) {
				int[] newSol = (int[]) currentSol.Clone();
				int j = r.Next(n);
				int i = r.Next(m);
				newSol[j] = i;
				if(capLeft[i] >= GAP.req[i,j]) {
					int newCost = currentCost - GAP.cost[currentSol[j],j] + GAP.cost[i,j];

					if(newCost <= currentCost) {
						capLeft[currentSol[j]] += GAP.req[currentSol[j],j];
						capLeft[i] -= GAP.req[i,j];

						currentSol = newSol;
						currentCost = newCost;

						if(newCost < optCost) {
							optSol = newSol;
							optCost = newCost;
							step = 0;
						}
					} else {
						if(!T.HasValue) T = -(newCost - currentCost) / Math.Log(p);
						else p = Math.Exp(-(newCost - currentCost) / T.Value);

						if(r.Next() / (float) Int32.MaxValue < p) {
							capLeft[currentSol[j]] += GAP.req[currentSol[j],j];
							capLeft[i] -= GAP.req[i,j];

							currentSol = newSol;
							currentCost = newCost;
						}
					}
				}

				step++;
				totalSteps++;
				if(totalSteps % (n * (n - 1)) == 0) T *= 0.98;
			}
			return optSol;
		}

		/*
		 * Soluzione Tabu Search;
		 * 1. Genera una soluzione iniziale ammissibile S,
		      poni S* = S e inizializza TL=empty.
		   2. Trova S' ÎN(S), tale che
		      z(S')=min {z(S^), "S^in(S), S^Ï TL}.
		   3. S=S', TL=TL È {S}, se (z(S*) > z(S)) poni S* = S.
		   4. se not(end condition) go to step 2.
		 * Controlla tutto con algoritmo di ricerca locale, prendi la sol migliore(anche se è pggiore della corrente) 
         * e mettila come sol corrente, aggiungi la tupla alla tabu list e continua
		 * 
		 * Setto le capacità iniziali a quelle attuali per ogni server
		 * Inizializzo la soluzione 
		 * Creo la tupla della posizione i server e j client
		 * Pongo il costo temporaneo uguale a z
		 * Accetto soluzione anche se peggiorativa
		 * Caso in cui la soluzione non è tabù ed è minore della soluzione migliore locale
		 * Caso in cui la soluzione è tabu ma è migliore della soluzione più bella
		 * Assegno la nuova soluzione trovata
		 * Aggiungo la posizione che l'ha generata alla tabu list
		 * Se la soluzione trovata è migliorativa, il suo costo è il nuovo best absolute solution
		 * Assegno le nuove capacità sulla base della mossa che ho appena fatto
		 * Il costo della soluzione che prendo in considerazione al prossimo passo sarà pari a bestLocalSolution
		 */
		private int[] computeTabuSearch(int[] solution,int tabuTenure = 10,int maxIteration = 1000) {

			int bestAbsoluteSolution = checkSol(solution); //Miglior soluzione possibile, usato per accettare valori Tabu.
			int bestLocalSolution = Int32.MaxValue; //Miglior soluzione locale, usato dentro l'algoritmo
			int stepsToDo = maxIteration;
			Queue<Pair<int,int>> tabuQueue = new Queue<Pair<int,int>>(tabuTenure); //Coda per le mosse Tabu
			int[] tmpsol = (int[]) solution.Clone();
			int[] nextsol = (int[]) solution.Clone();
			Pair<int,int> bestPosition = new Pair<int,int>(-1,-1);
			int step = 0;

			int z = bestAbsoluteSolution;
			int isol = 0;
			int[] capleft = new int[m];
			int[] nextCapleft = new int[m];
			int[,] cost = GAP.cost;
			int[,] req = GAP.req;

			while(step < stepsToDo) {

				for(int j = 0;j < n;j++) {
					for(int i = 0;i < m;i++) {
						capleft = (int[]) capacitiesLeft.Clone();
						tmpsol = (int[]) solution.Clone();
						isol = tmpsol[j];
						Pair<int,int> currentPosition = new Pair<int,int>(i,j);
						int tempCost = z;
						if(i != isol && capleft[i] >= req[i,j]) {
							tmpsol[j] = i;
							capleft[i] -= req[i,j];
							capleft[isol] += req[isol,j];
							tempCost -= (cost[isol,j] - cost[i,j]);

							if(!tabuQueue.contains(currentPosition) && tempCost < bestLocalSolution) {
								bestLocalSolution = tempCost;
								nextsol = (int[]) tmpsol.Clone();
								bestPosition = currentPosition;
								nextCapleft = (int[]) capleft.Clone();

							}
							if(tabuQueue.contains(currentPosition) && tempCost < bestLocalSolution
								&& tempCost < bestAbsoluteSolution) {
								bestLocalSolution = tempCost;
								bestAbsoluteSolution = tempCost;
								nextsol = (int[]) tmpsol.Clone();
								bestPosition = currentPosition;
								nextCapleft = (int[]) capleft.Clone();
							}
						}
					}
				}

				solution = (int[]) nextsol.Clone();
				tabuQueue.add(bestPosition);
				if(bestLocalSolution < bestAbsoluteSolution) {
					bestAbsoluteSolution = bestLocalSolution;
				}
				capacitiesLeft = (int[]) nextCapleft.Clone();
				z = bestLocalSolution;
				step++;
			}
			return solution;
		}

		public int simulatedAnnealing() {
			this.constructiveEurFirstSol();
			sol = computeSimulatedAnnealing(sol);
			return checkSol(sol);
		}

		public int tabuSearch() {
			this.constructiveEurFirstSol();
			sol = computeTabuSearch(sol);
			return checkSol(sol);
		}
	}
}